# Info

Logspit is a tool for simulating application logging. Its main goal was to quickly setup a simulation of a production environment logging in order to test logging infrastructure.

Main features are

  * Poisson distribution of log entries, defined by an average log entries per second

Next on the list

  * Improve log entry generation performance
  * Multiple samples
  * Reduce busy wait in favor of `usleep`

## Environment Variables (AKA: application run-time settings)

  * `LOGSPIT_AVG_PER_SECOND` lets you set the average number of logs per second. The variation is based on probability of events and thus there is no min and max number, it all depends on probability. If you set this to 100 for example, you can easily reach 150-200 log bursts. Note that high throughput values are dependent on the speed at which your log entries can be generated.
  * `LOGSPIT_VERBOSE` lets you specify verbosity level. Verbosity is sent to STDERR, while the log entries output is sent to STDOUT.
    * A value of 0 will produce no extra output
    * A value of 1 or higher will output statistics when you terminate the program with SIGINT
    * A value of 2 or higher will output statistics every minute
    * A value of 3 or higher is not something you want to do
  * `LOGSPIT_SAMPLE_FOLDER` lets you specify the container folder which has the `sample.json` and `config.json` files in case you add your own to a folder other than `/app/data`.

## Samples

Samples are defined using two files

  * `config.json` contains the definition of your types of values. This is what produce random data in your logs.
  * `sample.json` is the template log entry

### The `config.json` file

There are currently 6 types of value-types

  * `random-string` generates a random string with one or more words of variable size
  * `number` generate a random number
  * `selector` selects one value from a list of values
  * `datetime` generate a datetime
  * `uuid` generate a uuid
  * `array` generate an array with variable number of elements from a list of values

**Example configuration**

```
{
  "message": {
    "type": "random-string",
    "max_words": 4,
    "min_words": 2,
    "max_size_per_word": 10
  },
  "now": {
    "type": "datetime",
    "format": "ISO8601"
  },
  "timestamp-in-the-last-7-days": {
    "type": "datetime",
    "format": "ISO8601",
    "range": {
      "max_days_ago": "7",
      "min_days_ago": "0"
    }
  },
  "random-int": {
    "type": "number",
    "decimal": 0,
    "min": 1,
    "max": 10000
  },
  "random-float": {
    "type": "number",
    "decimal": 2,
    "min": 1,
    "max": 10000
  },
  "uuid": {
    "type": "uuid"
  },
  "tags": {
    "type": "array",
    "min_size": 0,
    "max_size": 2,
    "values": ["one", "two", "three"]
  },
  "log-level": {
    "type": "selector",
    "values": ["debug", "info", "warning", "error", "fatal"]
  }
}
```

### The `sample.json` file

**Example**

```
{
  "msg": "{%message%}",
  "tags": "{%tags%}",
  "level": "{%log-level%}",
  "value": {%random-float%},
  "version": {%random-int%},
  "timestamp": "{%timestamp-in-the-last-7-days%}",
  "dup-ctrl": "{%uuid%}"
}
```

## Run it

Outputing to a file to load onto Elasticsearch.

```
docker run \
  --rm \
  --name logspit \
  -v /your/log/sample/folder:/app/data \
  -e LOGSPIT_AVG_PER_SECOND=100 \
  vredens/logspit > log-samples.log
```

Using docker to output directly to graylog or logstash endpoints

```
docker run \
  --rm \
  --name logspit \
  --log-driver=gelf \
  --log-opt gelf-address=udp://localhost:12201 \
  vredens/logspit
```
