#!/usr/bin/env perl

use strict;
use warnings;
use v5.10;

use constant VERBOSE => $ENV{LOGSPIT_VERBOSE};

use Storable qw/dclone/;
use Time::HiRes qw/gettimeofday tv_interval alarm/;

use Data::GUID;
use JSON::XS;

my $opts = {
	avg    => $ENV{LOGSPIT_AVG_PER_SECOND} // 1,
	sample => $ENV{LOGSPIT_SAMPLE_FOLDER} // './sample',
};

my $src = $opts->{'sample'};

print "$src\n";

die "usage: LOGSPIT_AVG_PER_SECOND=1 LOGSPIT_SAMPLE_FOLDER=./sample $0" unless defined $src and -d $src;
die "no [$src/config.json] found" unless -f "$src/config.json";
die "no [$src/sample.json] found" unless -f "$src/sample.json";

my $sample = '';
my $config = '';
my $fh;

open($fh, '<', "$src/config.json") or die 'failed to open config file';
while(<$fh>) {
	chomp;
	$config .= $_;
}
close($fh);
open($fh, '<', "$src/sample.json") or die 'failed to open sample file';
while(<$fh>) {
	chomp;
	$sample .= $_;
}
close($fh);

$sample = decode_json($sample);
$config = decode_json($config);

validate_config($config);

my $ts = [gettimeofday];
my $t0 = [gettimeofday];
my $t1;
my $elapsed;
my $avg = $opts->{avg};
my $entry;
my $cnt = 0;
my $last_cnt = 0;

# clean exit
$SIG{INT} = sub {
	$elapsed = tv_interval($ts);
	say STDERR '[total:', $elapsed, 'ms][cnt:', $cnt, '][ravg:', $cnt / $elapsed, 'l/s][avg: ', $avg, 'l/s]' if VERBOSE;

	exit 0;
};

$SIG{ALRM} = sub {
	say STDERR 'last 60 seconds [events:', $cnt - $last_cnt, ']' if VERBOSE and VERBOSE > 1;
	$last_cnt = $cnt;
};

alarm(60, 60);

# generate logs using a poisson distribution
while (1) {
	$t1 = [gettimeofday];

	$entry = generate_entry(dclone($sample), $config);

	$elapsed = tv_interval($t0, $t1);

	# TODO: add an elapsed comparison to the desired average to see if we could
	# replace calculation of discarded entries with just a usleep instead and
	# save some cpu power / energy (Goooooooo PLANET!)

	my ($_r, $_v) = (rand(), $elapsed * $avg);
	#say '[elapsed: ', $elapsed, 'ms][rand:', $_r, '][val:', $_v, ']';
	if ($_r < $_v) {
		say encode_json($entry);
		$cnt++;
	}

	$t0 = $t1;
}


#===============================================================================
sub generate_entry {
#===============================================================================
	my ($sample, $config) = @_;

	foreach my $k (keys %$sample) {
		if (ref $sample->{$k} eq 'HASH') {
			generate_entry($sample->{$k}, $config);
		} elsif (ref $sample->{$k} eq 'ARRAY') {
			my @elems;
			foreach (@{$sample->{$k}}) {
				push @elems, generate_value($_, $config);
			}
			$sample->{$k} = \@elems;
		} else {
			$sample->{$k} = generate_value($sample->{$k}, $config);
		}
	}

	$sample;
}

#===============================================================================
sub generate_value {
#===============================================================================
	my ($placeholder_name, $config) = @_;

	if ($placeholder_name =~ m!\{%([-_\.\w]+)%\}!) {
		$placeholder_name = $1;
	} else {
		return $placeholder_name;
	}

	say STDERR 'generating value for leaf ', $placeholder_name if VERBOSE and VERBOSE > 2;

	die 'no such entry [', $placeholder_name, '] in your configuration file' unless defined $config->{$placeholder_name};

	my $placeholder = $config->{$placeholder_name};
	my $type = $placeholder->{type};
	my $generator;

	if ($type eq 'random-string') {
		$generator = \&generate_random_string;
	} elsif ($type eq 'selector') {
		$generator = \&generate_selector
	} elsif ($type eq 'datetime') {
		$generator = \&generate_datetime;
	} elsif ($type eq 'uuid') {
		$generator = \&generate_uuid;
	} elsif ($type eq 'array') {
		$generator = \&generate_array;
	} elsif ($type eq 'number') {
		$generator = \&generate_number;
	} else {
		die 'invalid placeholder type [', $type, ']';
	}

	&$generator($placeholder);
}

#===============================================================================
sub validate_config {
#===============================================================================
	my $config = shift;

	foreach my $n (keys %$config) {
		my $ph = $config->{$n};
		my $type = $ph->{type};

		if ($type eq 'array') {
			# validator
			foreach (qw/min_size max_size values/) { die 'missing ', $_ unless defined $ph->{$_}; }
			die 'max_size must be higher or equal than min_size' unless $ph->{max_size} >= $ph->{min_size};
			die 'min_size must be 0 or higher' unless $ph->{min_size} >= 0;
			die 'max_size must be 1 or higher' unless $ph->{max_size} > 0;
			die 'values must have a length of at least max_size' unless @{$ph->{values}} >= $ph->{max_size};

		} elsif ($type eq 'random-string') {
			foreach (qw/max_words min_words max_size_per_word/) { die 'missing ', $_ unless defined $ph->{$_}; }
			die 'min_words must be higher than 1' unless $ph->{min_words} > 0;
			die 'max_words must be higher than min_words' unless $ph->{max_words} > $ph->{min_words};
			die 'max_size_per_word must be greater than 1' unless $ph->{max_size_per_word} > 1;

		} elsif ($type eq 'uuid') {
			# nothing

		} elsif ($type eq 'selector') {
			foreach (qw/values/) { die 'missing ', $_ unless defined $ph->{$_}; }
			die 'values must be an array with at least one item' unless ref $ph->{values} eq 'ARRAY' and @{$ph->{values}} > 0;

		} elsif ($type eq 'number') {
			foreach (qw/min max decimal/) { die 'missing range.', $_ unless defined $ph->{$_}; }
			die 'max must be higher than min' unless $ph->{max} > $ph->{min};
			die 'decimal must be a value between 0 and 6' unless $ph->{decimal} >= 0 and $ph->{decimal} <= 6;

		} elsif ($type eq 'datetime') {
			# nothing
			if (defined $ph->{format}) {
				die 'invalid datetime format [', $ph->{format}, ']' unless grep { $_ eq $ph->{format}; } qw/ISO8601/;
			}
			if (defined $ph->{range}) {
				my $range = $ph->{range};
				foreach (qw/max_days_ago min_days_ago/) { die 'missing range.', $_ unless defined $range->{$_}; }
				die 'range.min_days_ago must be 0 or higher' unless $range->{min_days_ago} >= 0;
				die 'range.max_days_ago must be 0 or higher' unless $range->{max_days_ago} >= 0;
				die 'range.max_days_ago must be equal or higher than range.min_days_ago' unless $range->{max_days_ago} >= $range->{min_days_ago};
			}

		} else {
			die 'invalid placeholder type [', $type, ']';
		}
	}
}

#===============================================================================
sub _random {
#===============================================================================
	my ($m, $M) = @_;

	int(rand() * ($M - $m + 1) + $m);
}

#===============================================================================
sub _random_float {
#===============================================================================
	my ($m, $M) = @_;

	rand() * ($M - $m + 1) + $m;
}

#===============================================================================
sub generate_array {
#===============================================================================
	# TODO: iterate over the array and select fields based on probability of select: rand() > desired_length/array_length
	my ($ph, $n) = @_;

	my @res;
	my ($M, $m) = ($ph->{max_size}, $ph->{min_size});
	my $l = _random($m, $M);

	for (my $i = 0; $i < $l; $i++) {
		push @res, $ph->{values}->[_random(0, scalar @{$ph->{values}} - 1)];
	}

	\@res;
}

#===============================================================================
sub generate_random_string {
#===============================================================================
	my $ph = shift;
	my $n_words = _random($ph->{min_words}, $ph->{max_words});

	my @words;
	for (my $i = 0; $i < $n_words; $i++) {
		push @words, generate_word(_random(1, $ph->{max_size_per_word}));
	}

	join(' ', @words);
}

#===============================================================================
sub generate_word {
#===============================================================================
	my $l = shift;

	my $w = '';
	for (my $i = 0; $i < $l; $i++) {
		$w .= chr(_random(0, 25) + ord('a'));
	}

	$w;
}

#===============================================================================
sub generate_uuid {
#===============================================================================
	return Data::GUID->new->as_string;
}

#===============================================================================
sub generate_selector {
#===============================================================================
	my $ph = shift;

	my $sel = _random(0, scalar @{$ph->{values}} - 1);

	$ph->{values}->[$sel];
}

#===============================================================================
sub generate_datetime {
#===============================================================================
	# TODO: replace this with Date::Manip
	my $ph = shift;

	my $datecmd = "date --iso-8601=sec --utc";
	if (defined $ph->{range}) {
		my ($md, $Md) = ($ph->{range}->{min_days_ago}, $ph->{range}->{max_days_ago});
		my ($h, $m, $s, $d) = (_random(0,24), _random(0, 59), _random(0, 59), _random($md, $Md));

		$datecmd .= " --date '$h hours ago $m minutes ago $s seconds ago $d days ago'";
	}

	my $res = `$datecmd 2>/dev/null`;
	die 'Error running [', $datecmd, ']' if $?;

	chomp $res;
	$res =~ s/\+0000$/Z/;

	$res;
}

#===============================================================================
sub generate_number {
#===============================================================================
	my $ph = shift;

	my ($min, $max, $decimal) = ($ph->{min}, $ph->{max}, $ph->{decimal});

	sprintf("%.${decimal}f", _random_float($min, $max));
}
