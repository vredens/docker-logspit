FROM perl:5.20
MAINTAINER JB Ribeiro <vredens@gmail.com>

ENV LOGSPIT_VERBOSE 0
ENV LOGSPIT_AVG_PER_SECOND 10
ENV LOGSPIT_SAMPLE_FOLDER "/app/data"

# install dependencies
COPY ./.provision /app/provision
RUN [ "/app/provision/install.sh" ]

# copy "application"
COPY ./samples/example01 /app/data
COPY ./src /app/src
WORKDIR /app/src

#VOLUME ["/app/data"]

# start it up
CMD [ "perl", "./logspit.pl" ]
