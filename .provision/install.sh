#!/bin/bash

if command -v cpanm 2>/dev/null; then
	echo 'CPANMinus installed'
elif curl -L http://cpanmin.us | perl - --sudo App::cpanminus; then
	echo 'CPANMinus installed'
fi

cpanm JSON::XS Data::GUID
